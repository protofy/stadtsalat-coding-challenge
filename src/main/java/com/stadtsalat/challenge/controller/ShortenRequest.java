package com.stadtsalat.challenge.controller;

public class ShortenRequest {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
